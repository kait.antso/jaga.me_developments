-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: jagame
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `item_name` varchar(200) NOT NULL,
  `number_of_items` int(11) DEFAULT NULL,
  `item_condition` varchar(45) DEFAULT NULL,
  `deal_type` varchar(45) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `image_url` varchar(200) DEFAULT NULL,
  `date_item` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_item_category_idx` (`category_id`),
  KEY `fk_item_user_idx` (`user_id`),
  KEY `fk_item_location_idx` (`location_id`),
  CONSTRAINT `fk_item_category` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_item_location` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_item_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=219 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,35,1,3543,'Kott pahtlit',1,'kasutatud','ära anda','Jäi üle kott pahtlit, avastin. Aga ei läinud vaja.','cdec262279a756ec1137a7456b7cc006.jpg','2018-03-08 18:25:00'),(2,31,2,666,'Vanad laudauksed',2,'null','null','Laut lagunes ära, aga vanad tulekindlad uksed jäid alles.','f1aecbe65023e1768173f6d70cd438d0.jpg','2018-03-27 13:03:00'),(4,29,2,745,'Trepp',2,'null','null','Laut lagunes ära, trepp jäi alles. Annan ära.','b7b3f060-ddee-4574-ac7a-1a10cfba6412.jpg','2018-03-27 14:38:00'),(6,28,2,666,'Korsten',1,'Seisab kindlalt püsti veel.','null','Hea seisukorras, lihtne demonteerida.','20023.jpg','2018-03-31 17:02:00'),(7,37,2,742,'Naelad',7,'peaaegu uued','null','Üsna heas seisuskorras ja peaaegu sirged','1_9_15552849.jpg','2018-03-31 17:03:00'),(14,39,2,788,'Universaalklamber',1,'normaalne','null','multiotstarbeline metallist konks','29fb77e4-1517767976-518-e.jpg','2018-03-31 17:03:00'),(18,48,2,666,'Vana lüliti',1,'väga vana','null','Pätsuaegne vana lüliti. Keraamiline. Üliilus.','1_1_16424994.jpg','2018-03-31 17:03:00'),(21,41,2,666,'WC pott',1,'Veidi kasutatud','null','Vajab kerget puhastust','0dfa69ad-2ce0-47a8-89ff-9f183ec86d74.jpg','2018-03-29 15:41:00'),(22,31,2,666,'uksed',2,'hea','annan ära','vanad puidust ukseds','050ec27b-3d2d-42aa-af5a-823e86b83f32.jpg','2018-03-23 10:40:00'),(51,44,18,3981,'Käsipuur koos puuriga',1,'Hea','null','Antiikne käsipuur.','18fa0351-6345-42a9-9106-c14d20c81350.jpg','2018-03-27 15:05:00'),(52,48,1,3344,'Pistikupesa Wifi ühendusega',1,'uus','null','Ei leidnud kasutust','1f1412d1-9481-4d1a-8cf6-e2b09d5f99cb.jpg','2018-03-27 13:24:00'),(53,42,1,861,'Ioonkütte katel',1,'keskmine','null','','65d9470a-d745-414f-8289-d38335745b0a.jpg','2018-03-27 13:34:00'),(54,35,1,374,'vuugitäide',1,'pool on alles','null','','63309da9-02b0-488a-a48b-3fc2ddf88160.jpg','2018-03-27 13:38:00'),(55,38,23,475,'Neli seibi',4,'vanad','null','Leidsin sahtlist, annan ära','5b13122b-392b-414b-b7cf-f974d7b6cdac.jpg','2018-03-27 13:42:00'),(56,33,23,1278,'Tapeedirullid',5,'vanad','null','','ae0d2f9b-41f4-4cc2-9688-e50a38e5c595.jpg','2018-03-27 13:44:00'),(57,36,23,1712,'liim',1,'Veidi kasutatud','null','','f38f1ff3-5e5d-45de-91cb-beda2df1b384.jpg','2018-03-27 13:50:00'),(58,43,17,3000,'akulöök-trell',1,'','null','Info saamiseks, helista ja küsi lisa','f931f1dc-09eb-42ea-af48-2b9eb81a8d9d.jpg','2018-03-27 13:54:00'),(59,46,17,3000,'vana lamp',1,'okei','null','','a8958436-07cd-4131-8c36-733213be0beb.jpg','2018-03-27 14:05:00'),(60,30,24,2341,'Profiilplekk',6,'Sisuliselt uus','null','Katuseplekk, erinevat värvi','ee92dd80-3816-4a90-a525-63a985803a5c.jpg','2018-03-27 14:35:00'),(61,32,25,3696,'Kivivill',4,'Korra seinas käinud','null','4 kivivilla plaati','5f641fb6-d771-4555-8ab5-3f9ea7e16631.jpg','2018-03-27 14:45:00'),(62,47,18,3646,'Kõrgepingekaabel',20,'Korralikult sissetöötatud','null','Sobiva ristlõikega, uus omanik peab ise deinstallima','a948de82-3a3e-4121-9b8d-3b01b2adb7b6.jpg','2018-03-27 15:08:00'),(63,40,24,2341,'Kanalisatsioonitorud',8,'Korra maasse pandud, midagi läbi pole lastud','null','8m pikkused plastiktorud','3f219488-1f4b-4d78-bf92-5989a0c40d5b.jpg','2018-03-27 15:11:00'),(64,34,26,2684,'Põrandalakk',6,'4 purki kinni, kahel pealt natuke puudu ','null','Hea lakk, usaldusväärselt tootjalt','8ced13d8-1d38-4e62-9cbb-9302d4a16bcb.jpg','2018-03-27 15:36:00'),(65,45,26,2684,'Elektrikaitsmed',7,'Mõned uued, mõned kasutatud','null','Erineva amperaaziga, igale maitsele','b95a08a8-aa54-4df9-969a-f9e2346db481.jpg','2018-03-27 15:40:00'),(66,43,26,2684,'Segumasin',1,'Töökorras','null','Hästi hoitud, korralised hooldused tehtud','bc312f26-b3be-4c79-90d2-2c73b5991016.jpg','2018-03-27 15:43:00'),(67,28,3,567,'Savitellised',3,'Kasutatud, puhastatud','null','3 alust korralikke savitelliseid','cda3d72b-6f91-4301-9edc-ca4a9cc461fc.jpg','2018-03-27 15:49:00'),(68,30,3,567,'Tõrvapapp',23,'Uued','null','Pool aastat kuuris seisnud, kasutamata','b006c492-882b-4d44-9fcd-200fbf9ae50c.jpg','2018-03-31 17:31:00'),(69,29,3,567,'Palkmaja detailid',93,'Uued','null','Palkmaja karkassi komplekt','6921f2fb-76da-4a03-b00f-798173a001e3.jpg','2018-03-31 17:31:00'),(70,39,22,3646,'Sepishinged',3,'Kõik on uued','null','Käsitöö, väga kõrge kvaliteet','a2fcd034-04be-4a48-ae31-4b7807160d4c.jpg','2018-03-27 16:01:00'),(71,47,22,3646,'Jõukaabel',1,'Väge vähe kasutuses olnud','null','25m pikenduskaabel, 3-faasiga','6789f4bf-a142-4a3c-8456-dca7e88c5e0a.jpg','2018-03-27 16:11:00'),(209,34,110,668,'Värvipurgid',10,'pooleldi kasutatud','null','Umbes kümmekond purki, pooltäis. Normaalne värv, importkaup.','4ec5c01b-47e9-4533-a572-a0be7ab9eb1a.jpg','2018-03-31 16:51:00'),(210,41,110,668,'Kraanikauss',1,'Uueväärne','null','Eksklusiivne kraanikuauss. Tšehhi toode.','008653b5-4503-4118-b1a5-af458def007a.jpg','2018-03-31 16:52:00'),(211,29,110,668,'Põrandaliist',1,'Kasutatud.','null','Hiir näris augu sisse. Muidu nagu uus.','17610522-d997-4d8d-8c14-e28cb30b8a69.jpg','2018-03-31 16:55:00'),(212,41,110,668,'Valamukapp',1,'Peaaegu uus.','null','Valamukapp kraanikuausiga. Kapis saab hoida näiteks majapidamistarbeid, pesupulbreid ja muid hädavajalikke asju. Kasutaks ise, aga mul on kodus nii vähe ruumi ju.','c0e5fc3c-6337-4b0e-849f-4486781623bb.jpg','2018-03-31 16:58:00'),(213,40,110,668,'Torud',2,'Vanad','null','Kollektsionäärile. Roostes.','a6cb9671-8ff5-4a84-9b2d-476708077eb5.jpg','2018-03-31 16:59:00'),(214,36,110,668,'Kummiliim',1,'Vana','null','Lõhnab endiselt hästi.','6a8c66b3-d088-4ebb-984c-85527e7ae30a.jpg','2018-03-31 17:24:00'),(215,44,110,668,'Kellu',1,'Veidi kasutatud.','null','Mugav tööriist. Ehitasin ämmale uue välikäimla sellega.','939aa1cf-6cca-44d2-85d3-d9d8b4eda34a.jpg','2018-03-31 17:25:00'),(216,42,110,668,'Elektriradikas',1,'Kasutatud','null','Töötab. Annab rohkem sooja kui oma naine.','f5716ded-7647-4dc3-8810-e4e5b9e90f72.jpg','2018-03-31 17:27:00'),(217,37,110,668,'Kipsikruvid',1,'Uus','null','Üks rull kipsikruve.','07f4c472-7446-403b-9a55-0a0d09334780.jpg','2018-03-31 17:28:00'),(218,38,110,668,'Rauakola',800,'Vana','null','Terve virn vanu mutreid ja polte ja seibe. Vii ära ja pane albumisse.','12d545b9-59ce-4b01-9c42-573395e99912.jpg','2018-03-31 17:31:00');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-02  9:50:37
