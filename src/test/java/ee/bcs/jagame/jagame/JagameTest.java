package ee.bcs.jagame.jagame;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ee.bcs.jagame.jagameDAO.Category;
import ee.bcs.jagame.jagameDAO.Item;
import ee.bcs.jagame.jagameDAO.Location;
import ee.bcs.jagame.jagameDAO.User;

public class JagameTest {
	
	String lastUserId = "";
	int lastUserIdInt = 0;
	String lastItemId = "";

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {
		Utilities.consumeSQL("DELETE FROM items WHERE user_id =" + lastUserIdInt);
		Utilities.consumeSQL("DELETE FROM users WHERE id =" + lastUserIdInt);
	}

	@Test
	public void testGetCategories() throws SQLException {
		Category[] categories = CategoryService.getCategories();
		assertTrue(categories != null);
	}
	
	@Test
	public void testGetSubCategories() {
		Category[] subCategories = CategoryService.getSubCategories("3");
		assertTrue(subCategories != null);
	}
	
	@Test
	public void testGetSiblingCategories() {
		Category[] siblingCategories = CategoryService.getSiblingCategories("38");
		assertTrue(siblingCategories != null);
	}
	
	@Test
	public void testGetCategoryByItem() {
		Category[] categoriesByItemId = CategoryService.getCategoryByItem("1");
		assertTrue(categoriesByItemId != null);
		assertTrue((categoriesByItemId[0].getCategory_name()).equals("Seinakatted"));	
	}
	
	@Test
	public void testGetParentCategoryBySubCategory() {
		Category parentCategory = CategoryService.getParentCategoryBySubCategory("33");
		assertTrue((parentCategory.getCategory_name()).equals("Viimistlus"));
	}
	
	@Test
	public void testGetItems() {
		Item[] allItems = ItemService.getItems(0);
		assertTrue(allItems[0].getId() > 0);
		Item[] itemsByLocation = ItemService.getItems(1);
		assertTrue(itemsByLocation[0].getId() > 0);
	}
	
	@Test
	public void testGetUserItems() {
		Item[] userItems = ItemService.getUserItems(1);
		assertTrue(userItems[0].getId() > 0);	
	}
	
	@Test
	public void testGetParentCategoryItems() {
		Item[] parentCategoryItems = ItemService.getParentCategoryItems("1", 0);
		assertTrue(parentCategoryItems[0].getId() > 0);
		Item[] parentCategoryItemsWithLocation = ItemService.getParentCategoryItems("1", 3);
		assertTrue(parentCategoryItemsWithLocation[0].getId() > 0);
	}
	
	@Test
	public void testGetCategoryItems() {
		Item[] categoryItems = ItemService.getCategoryItems("28", 0);
		assertTrue(categoryItems[0].getId() > 0);
		Item[] categoryItemsWithLocation = ItemService.getCategoryItems("28", 15);
		assertTrue(categoryItemsWithLocation[0].getId() > 0);
	}
	
	@Test
	public void testGetItem() {
		Item item = ItemService.getItem("1");
		assertTrue(item.getId() > 0);
	}
	
	@Test
	public void testAddEditDeleteUsersAndItems() {
		
		User addUser = new User();
		addUser.setFirst_name("Test");
		addUser.setLast_name("Testson");
		addUser.setUser_name("test");
		addUser.setPassword("test");
		addUser.setPhone("55");
		addUser.setEmail("test");
		addUser.setLocation_id(111);
		UserService.addUsers(addUser);
		
		ResultSet result = Utilities.consumeSQL("SELECT MAX(id) FROM users");
		assertTrue(result != null);
		try {
			assertTrue(result.next());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			lastUserId = result.getString(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		lastUserIdInt = Integer.parseInt(lastUserId);
		
		Item saveItem = new Item();
		saveItem.setCategory_id(28); 
		saveItem.setUser_id(lastUserIdInt); 
		saveItem.setLocation_id(111); 
		saveItem.setItem_name("xxxxx"); 
		saveItem.setNumber_of_items(111);
		saveItem.setItem_condition("xxxxx"); 
		saveItem.setDeal_type("xxxxx"); 
		saveItem.setDescription("xxxxx"); 
		saveItem.setImage_url("xxxxx"); 
		ItemService.saveItem(saveItem);
		Item[] userItems = ItemService.getUserItems(lastUserIdInt);
		assertTrue(userItems[0].getId() > 0);
		
		ResultSet lastItem = Utilities.consumeSQL("SELECT MAX(id) FROM items");
		assertTrue(lastItem != null);
		try {
			assertTrue(lastItem.next());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			lastItemId = lastItem.getString(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		int lastItemIdInt = Integer.parseInt(lastItemId);
		
		Item editItem = ItemService.getItem(lastItemId);
		assertTrue(editItem != null);
		editItem.setItem_name("yyyyy");
		ItemService.saveItem(editItem);
		assertTrue((editItem.getItem_name()).equals("yyyyy"));	
		
		ItemService.deleteItem(saveItem);
		Item[] deleteSavedItem = ItemService.getUserItems(lastItemIdInt);
		assertTrue(deleteSavedItem != null);
		assertFalse(deleteSavedItem.length > 0);
		
		ItemService.deleteUserItems(saveItem);
		Item[] deletedUserItems = ItemService.getUserItems(lastUserIdInt);
		assertFalse(deletedUserItems.length > 0);
		
		User editUser = UserService.getUser(lastUserIdInt);
		assertTrue(editUser != null);
		editUser.setUser_name("JohnSmith");
		UserService.editUsers(editUser);
		assertTrue((editUser.getUser_name()).equals("JohnSmith"));
		
		UserService.deleteUser(addUser);
		User deleteAddedUser = UserService.getUser(lastUserIdInt);
		assertTrue(deleteAddedUser != null);
		
	}
	
	@Test
	public void testGetUser() {
		User userByName = UserService.getUser("uluk");
		assertTrue((userByName.getUser_name()).equals("uluk"));
		User userByUserName = UserService.getUser("uluk", "ulukurkull666");
		assertTrue((userByUserName.getUser_name()).equals("uluk"));
	}
	
	@Test
	public void testGetSubLocations() {
		Location[] sublocations = LocationService.getSubLocations();
		assertTrue(sublocations != null);
	}

	@Test
	public void testgetLocation() {
		Location location = LocationService.getLocation(153);
		assertTrue(location.getAdministrative_unit().equals("Krei küla"));
	}

	@Test
	public void testGetCounties() {
		Location[] getCounties = LocationService.getCounties();
		assertTrue(getCounties[0].getAdministrative_unit() != null);
	}
}