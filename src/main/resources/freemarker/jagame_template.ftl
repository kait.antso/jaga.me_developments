<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="//code.jquery.com/jquery-1.12.4.js"></script>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="/jagamewebapp/utilities.js"></script>

<title>Jaga.me jagamisrakendus</title>

 <style>
    
    .navbar {      
	    margin-bottom: 20px;      
	    border-radius: 4px;    
    }        
        	 
   	body {        
    	font-family: Arial, Helvetica, sans-serif;        
    	background-color: #eee;    
    }        
    	 
    .btn {    
    	cursor: pointer;    
    	display: inline-block;        
   	}    
    	 
   	.btn-hover {    
    	background: #eee;    
   	}    
    	  
   	h1 {
		font-family Arial, Helvetica, sans-serif;
		text-align: left;
		font-size: 50px;
		padding: 10px;
		margin-top: 30px;
	}  
	
	.ui-autocomplete {
		z-index: 1050;
	}            
    
    <#if page != "logIn" && page != "registration">	
                  
    #allItems {      
    	margin-left: 30px; 
    	margin-right: 30px;           
    }     
         
    </#if>
    	  
	<#if page == "logIn" || page == "registration">
	
	input[type=text], input[type=password] {
		width: 400px;
		padding: 12px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		box-sizing: border-box;
	}
	
	#userForm {
		float: left;
		width: 90%;
		margin-left: 30px;
	}
	
	</#if>
	
</style>
</head>

<body class="w3-light-grey w3-content">

<div class="ui-widget"></div>
<div class="w3-main">

<!-- Lehe päis-->

<header>         
	<div class="w3-container" >     
		<a href="/jagamewebapp/web/index">    
			<h1 style="color:grey"><b><strong>Jaga.me</strong></b></h1>
		</a>         
	</div>
</header>

<#if page != "logIn" && page != "registration">	

<!-- Lehe päise nupud-->
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="/jagamewebapp/web/index">Avaleht</a></li>
        <#if page != "myItems" && page != "userItems" && page != "item">
        <li style="margin-top:8px"><select class="form-control" id="countiesList" onchange="setCounty(currentPage)"></select></li>
        </#if>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      	<li id="registrateNewUser" data-toggle="modal"><a href="/jagamewebapp/web/registration">Registreeru kasutajaks</a></li>
      	<li id="addItemLinkPublic" data-toggle="modal" data-target="#registrate"><a href="#">Lisa kuulutus</a></li>
        <li id="addItemLink" data-toggle="modal" data-target="#addItemModal" style="display:none"><a href="#">Lisa kuulutus</a></li>
        <li id="editItemsLink" style="display:none"><a id="editItemsHyperlink" href="/jagamewebapp/web/my_items">Vaata oma kuulutusi</a></li>
        <li id="editUserLink" data-toggle="modal" data-target=""  style="display:none"><a href="/jagamewebapp/web/registration">Muuda kasutajat</a></li>
        <li id="logInLink"><a href="/jagamewebapp/web/login"><span class="glyphicon glyphicon-user"></span> Logi sisse</a></li>
        <li id="logOutLink" style="display:none" onclick="userLogOut()"><a href="#"><span class="glyphicon glyphicon-user"></span> Logi välja</a></li>
      </ul>
    </div>
  </div>
</nav>

<!-- Registreerimise modaal -->
<div class="modal fade" id="registrate" tabindex="-1" role="dialog" aria-labelledby="registrateLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<div align="center"><p>Kuulutusi saavad lisada ainult registreerunud kasutajad.</p></div>		
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" style="background-color: #eee" data-dismiss="modal" onclick="location.href='/jagamewebapp/web/login'">Logi sisse</button>
				<button type="button" class="btn btn-default" style="background-color: #eee" data-dismiss="modal" onclick="location.href='/jagamewebapp/web/registration'">Registreeru kasutajaks</button>
			</div>
		</div>
	</div>
</div>

<!-- Lisa kuulutus sisestusvälja sisu -->
<div class="modal fade" id="addItemModal" tabindex="-1" role="dialog" aria-labelledby="addItemModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="addItemModalLabel">Kuulutuse andmed</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<form enctype="multipart/form-data">
					<select class="form-control" id="modalCategories" onchange="getChildCategories()"></select><br>
					<select class="form-control" id="modalChildCategories" name="modalChildCategories"></select><br>
					<input type="text" class="form-control" id="location" name="location" placeholder="Asukoha nimi*"><br>
					<input type="text" class="form-control" id="item_name" name="item_name" placeholder="Eseme nimetus (kuni 200 tähemärki)*" maxlength="200"><br> 
					<input type="number" class="form-control" id="number_of_items" name="number_of_items" placeholder="Esemete arv*"><br> 
					<input type="text" class="form-control" id="item_condition" name="item_condition" placeholder="Eseme seisund (kuni 45 tähemärki)" maxlength="45"><br>
					<input type="text" class="form-control" id="description" name="description" placeholder="Eseme kirjeldus (kuni 1000 tähemärki)" maxlength="1000"><br>
					<p>Lae üles foto:</p> 
					<input type="file" class="form-control" id="image_url" name="image_url"><br>
					<input type="hidden" id="user_id" name="user_id">
					<input type="hidden" id="item_id" name="item_id">
					<p><font color="red" style="display:none" id="fillRequiredItemValues">Kohustuslik on valida eseme kategooria ja täita tärniga märgitud väljad.</font></p>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" style="background-color: #eee" onclick="saveItem()">Salvesta</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Sulge</button>
				<button type="button" class="btn btn-default" onclick="clearAddItemForm()">Tühjenda väljad</button>
			</div>
		</div>
	</div>
</div>
	
<!-- Kuulutuse kustutamise modaal -->
<div class="modal fade" id="deleteItemModal" tabindex="-1" role="dialog" aria-labelledby="deleteItemLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<div align="center"><p>Oled kindel, et soovid kuulutust kustutada?</p></div>	
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="deleteItem()">Kustuta</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Tühista</button>
			</div>
		</div>
	</div>
</div>
	
</#if>

<#if page == "logIn" || page == "registration">

<!-- Lehe päise nupud-->
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
       <li><a href="/jagamewebapp/web/index">Avaleht</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      	<#if page == "logIn">
      	<li id="registrateNewUser" data-toggle="modal"><a href="/jagamewebapp/web/registration">Registreeru kasutajaks</a></li>
      	</#if>
      	<#if page == "registration">
      	<li id="editItemsLink" style="display:none"><a id="editItemsHyperlink" href="/jagamewebapp/web/my_items">Vaata oma kuulutusi</a></li>
        <li id="logInLink"><a href="/jagamewebapp/web/login"><span class="glyphicon glyphicon-user"></span> Logi sisse</a></li>
        <li id="logOutLink" style="display:none" onclick="userLogOut()"><a href="#"><span class="glyphicon glyphicon-user"></span> Logi välja</a></li>
      	</#if>
      </ul>
    </div>
  </div>
</nav>

</#if>

<#if page == "logIn">

<!-- Login konteiner -->
<div class="container" id=userForm>
      <label for="username"><b>Kasutaja:</b></label><br>
      <input class="form-control" style="height:45px"  type="text" placeholder="Sisesta kasutaja" id="user_name" required><br>
      <label for="password"><b>Parool:</b></label><br>
      <input class="form-control" style="height:45px" type="password" placeholder="Sisesta parool" id="password" required><br>  
      <button type="submit" style=width:150px class="btn btn-default" onclick="userLogin()">Logi sisse</button><br>
      <p><font color="red" style="display:none" id="wrongPassword">Vale kasutajatunnus või parool</font></p>
</div>

</#if>

<#if page == "registration">

<!-- Registreerimise konteiner -->
<div class="container" id=userForm>
	<label for="firstName"><b>Eesnimi:</b></label><br> 
	<input class="form-control" style="height:45px"   type="text" placeholder="Sisesta eesnimi" id="registration_first_name" required><br>
	<label for="lastName"><b>Perekonnanimi:</b></label><br>
	<input class="form-control" style="height:45px"  type="text" placeholder="Sisesta perekonnanimi" id="registration_last_name" required><br> 
	<label for="location"><b>Asukoht:</b></label><br>
	<input class="form-control" style="height:45px"  type="text" placeholder="Sisesta asukoht" id="location" required> <br> 
	<label for="username"><b>Kasutaja:</b></label><br>
	<input class="form-control" style="height:45px"   type="text" placeholder="Sisesta kasutaja" id="registration_user_name" required><br> 
	<p><font color="red" style="display: none" id="renameUser">Selline kasutajanimi on juba kasutusel. Vali uus kasutajanimi.</font></p>
	<label for="password"><b>Parool:</b></label><br>
	<input class="form-control" style="height:45px"   type="password" placeholder="Sisesta parool" id="registration_password" required><br> 
	<label for="password"><b>Korda parooli:</b></label><br> 
	<input class="form-control" style="height:45px" type="password" placeholder="Sisesta parool uuesti" id="registration_password2" required><br>
	<p><font color="red" style="display: none" id="passwordMatch">Sisestatud paroolid ei kattu</font></p>
	<label for="phone"><b>Telefon:</b></label><br> 
	<input class="form-control" style="height:45px" type="text" placeholder="Sisesta telefoninumber" id="registration_phone" required><br>
	<label for="email"><b>E-mail:</b></label><br> 
	<input class="form-control" style="height:45px" type="text"	placeholder="Sisesta e-maili aadress" id="registration_email" required><br>
	<button type="submit" style=width:150px class="btn btn-default" id="registrateUser" onclick="userRegistrate()">Registreeri</button>
	<button type="submit" style="width:100px; display:none" class="btn btn-default" id="editUser" onclick="editUser()">Muuda</button>
	<button type="submit" style="width:100px; display:none" class="btn btn-default"	id="deleteUser"	onclick="deleteUserWindow()" style="display:none">Kustuta</button><br>
	<p><font color="red" style="display: none" id="fillOutEveryRow">Täitke	kõik väljad</font></p>
	<br><br><br>
</div>

 <!-- Kasutaja kustutamise modaal -->
<div class="modal fade" id="deleteUserModal" tabindex="-1" role="dialog" aria-labelledby="deleteUserLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<div align="center"><p>Oled kindel, et soovid kasutajat kustutada?</p></div>		
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="deleteUser()">Kustuta</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Tühista</button>
			</div>
		</div>
	</div>
</div>

<!--Registreerimine õnnestunud modaal -->
<div class="modal fade" id="regCompleteModal" tabindex="-1" role="dialog" aria-labelledby="registrateLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<div align="center"><p>Registreerimine õnnestus!</p></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location.href='/jagamewebapp/web/login'">Logi sisse</button>
			</div>
		</div>
	</div>
</div>
	
<!-- Kasutajate andmete uuendamine õnnestus-->
<div class="modal fade" id="regUpdateModal" tabindex="-1" role="dialog" aria-labelledby="registrateLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<div align="center"><p>Kasutaja andmete muutmine õnnestus! </p></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location.href='/jagamewebapp/web/index'">OK</button>
			</div>
		</div>
	</div>
</div>

</#if>
	
<div align="center" id = "allCategories" class = "row"></div>
<div align="center" id = "subCategories" class = "row"></div><br>
<div id = "allItems" class = "row"></div>

<script>
var currentPage;

<#if page == "index">
	currentPage = "index";
	$(document).ready(function(){ 
		getCurrentCounty();
		getAllCategories();
		getLoggedInUser();
		fillAddItemModalCategories();
		getLocalities();
		localities();
		getAllItems();
	});
</#if>

<#if page == "subCategory">
	currentPage = "subCategory";
	$(document).ready(function() {
		getCurrentCounty();
		getCategoryItems();
		getAllCategories();
		getCategoriesByItemCategory();
		getLoggedInUser();
		fillAddItemModalCategories();
		getLocalities();
		localities();
	});
</#if>

<#if page == "parentCategory">
	currentPage = "parentCategory";
	$(document).ready(function(){
		getCurrentCounty();
		getParentCategoryItems();
		getAllCategories();
		getSubCategories();
		getLoggedInUser();
		fillAddItemModalCategories();
		getLocalities();
		localities();
	});
</#if>

<#if page == "userItems">
	$(document).ready(function(){ 
		getAllUserItems();
		getLoggedInUser();
		fillAddItemModalCategories();
		getLocalities();
		localities();
	});
</#if>

<#if page == "myItems">
	$(document).ready(function(){ 
		getLoggedInUser();
		getAuthenticatedUserItems();
		fillAddItemModalCategories();
		getLocalities();
		localities();
	});
</#if>

<#if page == "item">
	$(document).ready(function() {
		getItem();
		getAllCategories();
		getCategoryByItem();
		getLoggedInUser();
		fillAddItemModalCategories();
		getLocalities();
		localities();
		getCurrentCounty();
	});
</#if>

<#if page == "logIn">
	$(document).ready(function(){ 
		$("#password").on("keydown", function(e) {
			if (e.which == 13) {
				userLogin();
			}
		});
	});
</#if>

<#if page == "registration">
	$(document).ready(function(){ 
		getLoggedInUser();
		localities();
		getLocalities();
	});
</#if>
	
</script>
</body>
</html>