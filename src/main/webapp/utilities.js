var currentlyActiveItemId = 0;
var currentParentCategoryId = 0;
var currentLoggedInUser = 0;
var currentLocationName;
var currentUserName;
var currentUserLocationId = 0;
var locationTags = [];
var locationId = [];
var currentCountyId = 0;

//general functions

function getAllCategories() {
	$.ajax({
		type : "get",
		url : "/jagamewebapp/rest/get_allcategories",
		success : function(result) {
			$("#allCategories").empty();
			$("#allCategories").append('<div class="col"><div class="container"><div class="btn-group">');
			for (var i = 0; i < result.length; i++) {
				var parent_category_id = result[i].id;
				var parent_category = result[i].category_name;
				$("#allCategories").append('<a href = "/jagamewebapp/web/parentcategory?parent_category_id=' + parent_category_id + '" type="button" style=width:250px class="btn btn-default">' + parent_category + '</a>');
			}
			$("#allCategories").append('</div></div></div>')
		}		
	});		
}

function getLoggedInUser() {
	$.ajax({
		type : "get",
		url : "/jagamewebapp/rest/get_authenticated_user",
		success : function(result) {
			currentLoggedInUser = result.id;
			currentUserLocationId = result.location_id;
			currentUserName = result.user_name;
			if (currentLoggedInUser > 0) {
				$("#logInLink").hide();
				$("#addItemLinkPublic").hide();
				$("#addItemLink").show();
				$("#logOutLink").show();
				$("#editUserLink").show();
				$("#editItemsLink").show();
				$("#registrateNewUser").hide();
				$("#registrateUser").hide();
				$("#editUser").show();
				$("#deleteUser").show();
			}
		}
	});
}	

function getLocalities() {
	$.ajax({
		type:"get",
		url:"/jagamewebapp/rest/get_locations",
		success : function(result){
			for(var i = 0; i < result.length; i++) {
				var locationName = result[i].administrative_unit;
				var subLocationId = result[i].id;
				locationTags.push(locationName);
				locationId.push(subLocationId);
			}	
			renewUserData();
		}
	});
}

function localities() {
	$("#location").autocomplete({
		minLength : 2,
		source : function(request, response) {
		     var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(request.term), "i");
		     response($.grep(locationTags, function(item){
		       return matcher.test(item);
		     }) );
		}	
	});
}

function getCounties() {
	$.ajax({
		type : "get",
		url : "/jagamewebapp/rest/get_counties",
		success : function(result) {
			$("#countiesList").append('<option value="0">Kogu Eesti</option>');
			for (var i = 0; i < result.length; i++) {
				var id = result[i].id;
				var county_name = result[i].administrative_unit;
				if (id != currentCountyId) {
					$("#countiesList").append(
							'<option value="' + id + '">' + county_name
									+ '</option>');
				}
				if (id == currentCountyId) {
					$("#countiesList").append(
							'<option value="' + id + '" selected>'
									+ county_name + '</option>');
				}
			}
		}
	});
}

function getLocationToId(itemUserLocationId) {
	var idNumber = locationId.indexOf(itemUserLocationId);
	currentLocationName = locationTags[idNumber];
}

function getIdToLocalities() {
	var locationNumber = locationTags.indexOf($("#location").val());
	var locationIdNumber = locationId[locationNumber];
	$("#location").val(locationIdNumber);
}

function getCurrentCounty() {
	$.ajax({
		type:"get",
		url:"/jagamewebapp/rest/get_current_county",
		success : function(result) {
			currentCountyId = result.id;
			getCounties();
		}
	});
}

function setCounty(currentPage) {
	if ($("#countiesList").val() != 0) {
		$.ajax({
			type : "post",
			url : "/jagamewebapp/rest/set_county",
			data : {
				"location_id" : $("#countiesList").val()
			},
			success : function(result) {
				if (currentPage == "index") {
					getAllItems();
				}
				if (currentPage == "parentCategory") {
					getParentCategoryItems();
				}
				if (currentPage == "subCategory") {
					getCategoryItems();
				}
			}
		});
	} else {
		$.ajax({
			type : "post",
			url : "/jagamewebapp/rest/set_county",
			data : {
				"location_id" : null
			},
			success : function(result) {
				if (currentPage == "index") {
					getAllItems();
				}
				if (currentPage == "parentCategory") {
					getParentCategoryItems();
				}
				if (currentPage == "subCategory") {
					getCategoryItems();
				}
			}
		});
	}
}

function fillAddItemModalCategories() {
	$.ajax({
		type : "get",
		url : "/jagamewebapp/rest/get_allcategories",
		success : function(result) {
			$("#modalCategories").empty();
			$("#modalCategories").append('<option>Vali kategooria</option>');
			for (var i = 0; i < result.length; i++) {
				var parent_category_id = result[i].id;
				var parent_category = result[i].category_name;
				$("#modalCategories").append(
						'<option value="' + parent_category_id + '">'
								+ parent_category + '</option>');
			}
		}
	});

}

function getChildCategories() {
	currentParentCategoryId = $("#modalCategories").val();
	
	$.ajax({
		type : "get",
		url : "/jagamewebapp/rest/get_subcategories?parent_category_id=" + currentParentCategoryId,
		success : function(result) {
			$("#modalChildCategories").empty();
			$("#modalChildCategories").append('<option value="0">Vali alamkategooria</option>');
			for (var i = 0; i < result.length; i++) {
				var categoryId = result[i].id;
				var categoryName = result[i].category_name;
				$("#modalChildCategories").append(
						'<option value="' + categoryId + '">'
								+ categoryName + '</option>');
			}
		}
	});
}

function getParentCategoryForItemEdit(categoryId) {
	var currentParentCategoryIdEdit = 0;
	$.ajax({
		type : "get",
		url : "/jagamewebapp/rest/get_parentcategory_by_subcategory?category_id=" + categoryId,
		success : function(result) {
			currentParentCategoryIdEdit = result.id;
		}
	});	
	$.ajax({
		type : "get",
		url : "/jagamewebapp/rest/get_allcategories",
		success : function(result) {
			$("#modalCategories").empty();
			for (var i = 0; i < result.length; i++) {
				var parent_category_id = result[i].id;
				var parent_category = result[i].category_name;
				if (parent_category_id != currentParentCategoryIdEdit) {
					$("#modalCategories").append(
							'<option value="' + parent_category_id + '">'
									+ parent_category + '</option>');
				} else {
					$("#modalCategories").append(
							'<option value="' + parent_category_id + '" selected>'
									+ parent_category + '</option>');
				}

			}
		}
	});
}


function getSiblingCategoriesForItemEdit(categoryId) {
	$.ajax({
		type : "get",
		url : "/jagamewebapp/rest/get_sibling_categories?category_id=" + categoryId,
		success : function(result) {
			$("#modalChildCategories").empty();
			for (var i = 0; i < result.length; i++) {
				var siblingCategoryId = result[i].id;
				var siblingCategoryName = result[i].category_name;
				if (siblingCategoryId != categoryId) {
					$("#modalChildCategories").append(
							'<option value="' + siblingCategoryId + '">'
									+ siblingCategoryName + '</option>');
				} else {
					$("#modalChildCategories").append(
							'<option value="' + siblingCategoryId + '" selected>'
									+ siblingCategoryName + '</option>');
				}
				
			}
		}
	});	
}

function clearAddItemForm() {
	$("#modalCategories").val("");
	$("#modalChildCategories").val("");
	$("#location").val("");
	$("#item_name").val("");
	$("#number_of_items").val("");
	$("#item_condition").val("");
	$("#description").val("");
	$("#image_url").val("");	
}

function userLogOut() {
	$.ajax({
		type:"post",
		url:"/jagamewebapp/rest/authenticate",
		data: {
			"user_name":null,
			"password":null
		},
		success : function(result) {
			if (result == "notLoggedIn") {
				document.location = "/jagamewebapp/web/index";
			}	
		}
	});	
}

function saveItem() {
	$("#fillRequiredItemValues").hide();
	if ($("#location").val() == "" || $("#item_name").val() == ""
			|| $("#number_of_items").val() == "" || $("#modalChildCategories").val() == "0") {
		$("#fillRequiredItemValues").show();
	} else {
		getIdToLocalities();
		$.ajax({
			type : "post",
			dataType : "json",
			contentType : "application/json",
			url : "/jagamewebapp/rest/save_item",
			data : new FormData($('form')[0]),
			cache : false,
			contentType : false,
			processData : false,

			complete : function(result) {
				document.location = "/jagamewebapp/web/my_items";
				getAuthenticatedUserItems();
			}
		});
	}
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

//index functions

function getAllItems() {
	$.ajax({
			type : "get",
			url : "/jagamewebapp/rest/get_items",
			success : function(result) {
				$("#allItems").empty();
				var allItemsContainer = '<div class="row">';
				var maxLength = result.length > 6 ? 6 : result.length;
				for (var i = 0; i < maxLength; i++) {
					var id = result[i].id;
					var category_id = result[i].category_id;
					var user_id = result[i].user_id;
					var location_id = result[i].location_id;
					var item_name = result[i].item_name;
					var number_of_items = result[i].number_of_items;
					var item_condition = result[i].item_condition;
					var description = result[i].description;
					var image_url = result[i].image_url == "" ? "default_picture.jpg" : result[i].image_url;
					var date_item = result[i].date_item;
					var parent_category = result[i].parent_category;
					var sub_category = result[i].sub_category;
					var item_county = result[i].item_county;
					var item_locality = result[i].item_locality;
					var item_description = '<b> Kategooria: </b> ' + parent_category + ' - ' + sub_category + '<br>';
					item_description += '<b>Eseme nimetus: </b>' + item_name + '<br>';
					item_description += '<b>Kogus: </b>' + number_of_items + '<br>';
					item_description += '<b>Asukoht: </b>' + item_county + ', ' + item_locality + '<br>';
					item_description += '<a href = "/jagamewebapp/web/item?item_id=' + id + '">Vaata lähemalt</a>';
						
					var rowSwitch = '';
					if (i > 0 && i % 3 == 0) {
						rowSwitch = '</div><div class="row">';							
					}
					allItemsContainer += rowSwitch + '<div class="col-md-4"><div class="panel panel-primary" style="border-color:rgb(190, 190, 190)"><div class="panel-body"><img style=height:200px; width:200px src="/jagamewebapp/rest/get_image?image_name=' + image_url + '" class="img-responsive" style="width:100%" alt="Image"></div><div class="panel-footer">' + item_description + '</div></div></div>';
				}
				allItemsContainer += '</div>';
				$("#allItems").append(allItemsContainer);	
			}
		});
}

//parentcategory functions

function getSubCategories() {
	$.ajax({
		type : "get",
		url : "/jagamewebapp/rest/get_subcategories?parent_category_id=" + getParameterByName("parent_category_id"),
		success : function(result) {
			$("#subCategories").empty();
			$("#subCategories").append('<div class="col"><div class="container"><div class="btn-group">');
			for (var i = 0; i < result.length; i++) {
				var category_id = result[i].id;
				var category_name = result[i].category_name;
				$("#subCategories").append('<a href = "/jagamewebapp/web/subcategory?category_id=' + category_id + '" type="button" style="width:250px;background-color: inherit";border-color:rgb(190, 190, 190)" class="btn btn-default"><i>' + category_name + '</i></a>');
			}
			$("#subCategories").append('</div></div></div>')
		}
	});
}

function getParentCategoryItems(categoryId) {
	$.ajax({
		type : "get",
		url : "/jagamewebapp/rest/get_parentcategory_items?parent_category_id=" + getParameterByName("parent_category_id"),
		success : function(result) {
			$("#allItems").empty();
			var allItemsContainer = '<div class="row">';
			for (var i = 0; i < result.length; i++) {
				var id = result[i].id;
				var category_id = result[i].category_id;
				var user_id = result[i].user_id;
				var location_id = result[i].location_id;
				var item_name = result[i].item_name;
				var number_of_items = result[i].number_of_items;
				var item_condition = result[i].item_condition;
				var description = result[i].description;
				var image_url = result[i].image_url == "" ? "default_picture.jpg" : result[i].image_url;
				var date_item = result[i].date_item;
				var parent_category = result[i].parent_category;
				var sub_category = result[i].sub_category;
				var item_county = result[i].item_county;
				var item_locality = result[i].item_locality;
				var item_description = '<b> Kategooria: </b>' + parent_category + ' - ' + sub_category + '<br>';
				item_description += '<b>Eseme nimetus: </b>' + item_name + '<br>';
				item_description += '<b> Kogus: </b>' + number_of_items + '<br>';
				item_description += '<b> Asukoht: </b>' + item_county + ', ' + item_locality + '<br>';
				item_description += '<a href = "/jagamewebapp/web/item?item_id=' + id + '">Vaata lähemalt</a>';
				
				var rowSwitch = '';
				if (i > 0 && i % 3 == 0) {
					rowSwitch = '</div><div class="row">';							
				}
				
				allItemsContainer += rowSwitch + '<div class="col-md-4"><div class="panel panel-primary" style="border-color:rgb(190, 190, 190)"><div class="panel-body"><img style=height:200px; width:200px src="/jagamewebapp/rest/get_image?image_name=' + image_url + '" class="img-responsive" style="width:100%" alt="Image"></div><div class="panel-footer">' + item_description + '</div></div></div>';
			}
			allItemsContainer += '</div>';
			$("#allItems").append(allItemsContainer);	
		}
	});
}

//subcategory functions

function getCategoriesByItemCategory() {
	$.ajax({
		type : "get",
		url : "/jagamewebapp/rest/get_sibling_categories?category_id=" + getParameterByName("category_id"),
		success : function(result) {
			$("#subCategories").empty();
			$("#subCategories").append('<div class="col"><div class="container"><div class="btn-group">');
			for (var i = 0; i < result.length; i++) {
				var category_id = result[i].id;
				var category_name = result[i].category_name;
				$("#subCategories").append('<a href = "/jagamewebapp/web/subcategory?category_id=' + category_id + '" type="button" style="width:250px;background-color: inherit";border-color:rgb(190, 190, 190)" class="btn btn-default"><i>' + category_name + '</i></a>');
			}
			$("#subCategories").append('</div></div></div>')
		}
	});
}

function getCategoryItems() {
	$.ajax({
		type : "get",
		url : "/jagamewebapp/rest/get_category_items?category_id=" + getParameterByName("category_id"),
		success : function(result) {
			$("#allItems").empty();
			var allItemsContainer = '<div class="row">';
			for (var i = 0; i < result.length; i++) {
				var id = result[i].id;
				var category_id = result[i].category_id;
				var user_id = result[i].user_id;
				var location_id = result[i].location_id;
				var item_name = result[i].item_name;
				var number_of_items = result[i].number_of_items;
				var item_condition = result[i].item_condition;
				var description = result[i].description;
				var image_url = result[i].image_url == "" ? "default_picture.jpg" : result[i].image_url;
				var date_item = result[i].date_item;
				var parent_category = result[i].parent_category;
				var sub_category = result[i].sub_category;
				var item_county = result[i].item_county;
				var item_locality = result[i].item_locality;
				var item_description = '<b> Kategooria: </b>' + parent_category + ' - ' + sub_category + '<br>';
				item_description += '<b>Eseme nimetus: </b>' + item_name + '<br>';
				item_description += '<b>Kogus: </b>' + number_of_items + '<br>';
				item_description += '<b>Asukoht: </b>' + item_county + ', ' + item_locality + '<br>';
				item_description += '<a href = "/jagamewebapp/web/item?item_id=' + id + '">Vaata lähemalt</a>';
				
				var rowSwitch = '';
				if (i > 0 && i % 3 == 0) {
					rowSwitch = '</div><div class="row">';							
				}
				allItemsContainer += rowSwitch + '<div class="col-md-4"><div class="panel panel-primary" style="border-color:rgb(190, 190, 190)"><div class="panel-body"><img style=height:200px; width:200px src="/jagamewebapp/rest/get_image?image_name=' + image_url + '" class="img-responsive" style="width:100%" alt="Image"></div><div class="panel-footer">' + item_description + '</div></div></div>';
			}
			allItemsContainer += '</div>';
			$("#allItems").append(allItemsContainer);
		}
	});
}

//item functions

function getItem() {
	$.ajax({
		type : "get",
		url : "/jagamewebapp/rest/get_item?item_id=" + getParameterByName("item_id"),
		success : function(result) {
			$("#allItems").empty();
			var id = result.id;
			var category_id = result.category_id;
			var user_id = result.user_id;
			var location_id = result.location_id;
			var item_name = result.item_name;
			var number_of_items = result.number_of_items;
			var item_condition = result.item_condition;
			var description = result.description;
			var image_url = result.image_url == "" ? "default_picture.jpg" : result.image_url;
			var date_item = result.date_item;
			var parent_category = result.parent_category;
			var sub_category = result.sub_category;
			var item_county = result.item_county;
			var item_locality = result.item_locality;
			var user_name = result.user_name;
			var user_phone = result.user_phone;
			var user_email = result.user_email;
			var item_description = '<b>Kategooria: </b>' + parent_category + ' - ' + sub_category + '<br>';
			item_description += '<b>Eseme nimetus: </b>' + item_name + '<br>';
			item_description += '<b>Kogus: </b>' + number_of_items + '<br>';
			item_description += '<b>Asukoht: </b>' + item_county + ', ' + item_locality + '<br>';
			item_description += '<b>Seisukord: </b>' + item_condition + '<br>';				
			item_description += '<b>Kirjeldus: </b>' + description + '<br>';
			item_description += '<b>Kasutaja: </b><a href = "/jagamewebapp/web/user_items?user_id=' + user_id + '"> ' + user_name + ' </a><br>';
			item_description += '<b>Telefon: </b>' + user_phone + '<br>';
			item_description += '<b>E-mail: </b>' + user_email + '<br>';
			item_description += '<b>Lisamise kuupäev: </b>' + date_item + '<br>';
			$("#allItems").append('<div class="col-sm-6"> <div class="panel panel-primary"><div class="panel-body"><img src="/jagamewebapp/rest/get_image?image_name=' + image_url + '" class="img-responsive" style="width:100%" alt="Image"></div><div class="panel-footer">' + item_description + '</div></div></div>');
			}

	});
}

function getCategoryByItem() {
	$.ajax({
		type : "get",
		url : "/jagamewebapp/rest/get_category_by_item?item_id=" + getParameterByName("item_id"),
		success : function(result) {
			$("#subCategories").empty();
			$("#subCategories").append('<div class="col"><div class="container"><div class="btn-group">');
			for (var i = 0; i < result.length; i++) {
				var category_id = result[i].id;
				var category_name = result[i].category_name;
				$("#subCategories").append('<a href = "/jagamewebapp/web/subcategory?category_id=' + category_id + '" type="button" style="width:250px;background-color: inherit";border-color:rgb(190, 190, 190)" class="btn btn-default"><i>' + category_name + '</i></a>');
			}
			$("#subCategories").append('</div></div></div>')
		}
	});
}

//my_items functions

function getAuthenticatedUserItems() {
	$.ajax({
		type:"get",
		url:"/jagamewebapp/rest/get_authenticated_user_items",
		success : function(result){
			$("#allItems").empty();
			$("#allItems").append('<div style="margin-left:30px; font-size:120%;"><b>Sinu kõik kuulutused:</b></div><br>');
			var allItemsContainer = '<div class="row">';
			for (var i = 0; i < result.length; i++) {
				var id = result[i].id;
				var category_id = result[i].category_id;
				var user_id = result[i].user_id;
				var location_id = result[i].location_id;
				var item_name = result[i].item_name;
				var number_of_items = result[i].number_of_items;
				var item_condition = result[i].item_condition;
				var description = result[i].description;
				var image_url = result[i].image_url == "" ? "default_picture.jpg" : result[i].image_url;
				var date_item = result[i].date_item;
				var parent_category = result[i].parent_category;
				var sub_category = result[i].sub_category;
				var item_county = result[i].item_county;
				var item_locality = result[i].item_locality;
				var user_name = result[i].user_name;
				var user_phone = result[i].user_phone;
				var user_email = result[i].user_email;
				var item_description = '<b>Kategooria: </b>' + parent_category + ' - ' + sub_category + '<br>';
				item_description += '<b>Eseme nimetus: </b>' + item_name + '<br>';
				item_description += '<b>Kogus: </b>' + number_of_items + '<br>';
				item_description += '<b>Asukoht: </b>' + item_county + ', ' + item_locality + '<br>';
				item_description += '<b>Seisukord: </b>' + item_condition + '<br>';				
				item_description += '<b>Kirjeldus: </b>' + description + '<br>';
				item_description += '<b>Kasutaja: </b>' + user_name + ' <br>';
				item_description += '<b>Telefon: </b>' + user_phone + '<br>';
				item_description += '<b>E-mail: </b>' + user_email + '<br>';
	
				var rowSwitch = '';
				if (i > 0 && i % 3 == 0) {
					rowSwitch = '</div><div class="row">';							
				}
				allItemsContainer += rowSwitch + '<div class="col-md-4"> <div class="panel panel-primary" style="border-color:rgb(190, 190, 190)"><div class="panel-body"><img style=height:200px; width:200px src="/jagamewebapp/rest/get_image?image_name=' + image_url + '" class="img-responsive" style="width:100%" alt="Image"></div><div class="panel-footer">' + item_description + '<button style=width:100px class="btn btn-default" data-toggle="modal" data-target="#addItemModal" onclick="openEditItemModal(' + id + ',' + category_id + ',' + location_id + ')">Muuda</button><button style=width:100px class="btn btn-default" data-toggle="modal" onclick="openDeleteItemModal(' + id + ')">Kustuta</button></div></div></div>';	
			}
			$("#allItems").append(allItemsContainer);
		}	
	});
}

function openEditItemModal(itemId, categoryId, locationId) {
	getSiblingCategoriesForItemEdit(categoryId); 
	getParentCategoryForItemEdit(categoryId);
	getLocationToId(locationId);
	$("#item_id").val(itemId);
	$.ajax({
		type:"get",
		url:"/jagamewebapp/rest/get_item?item_id=" + itemId,
		success : function(result) {
			$("#location").val(currentLocationName);
			$("#item_name").val(result.item_name);
			$("#number_of_items").val(result.number_of_items);
			$("#item_condition").val(result.item_condition);
			$("#description").val(result.description);
			$("#image_url").val(result.image_url);	
			$("#user_id").val(currentLoggedInUser);
		}
	});
}

function openDeleteItemModal(itemId) {
	currentlyActiveItemId = itemId;
	$("#deleteItemModal").modal("show");
}

function deleteItem() {
	$.ajax({
		type : "post",
		dataType : "json",
		contentType : "application/json",
		url : "/jagamewebapp/rest/delete_item",
		data : JSON.stringify({
			"id": currentlyActiveItemId
		}),
		complete : function(result) {
			getAuthenticatedUserItems();
		}
	});
}

//user_items functions

function getAllUserItems(userId) {
	$.ajax({
		type : "get",
		url : "/jagamewebapp/rest/get_all_user_items?user_id=" + getParameterByName("user_id"),
		success : function(result) {
			$("#allItems").empty();
			$("#allItems").append('<div style="margin-left:30px; font-size:120%;"><b>Valitud kasutaja kõik kuulutused:</b></div><br>');
			var allItemsContainer = '<div class="row">';
			for (var i = 0; i < result.length; i++) {
				var id = result[i].id;
				var category_id = result[i].category_id;
				var user_id = result[i].user_id;
				var location_id = result[i].location_id;
				var item_name = result[i].item_name;
				var number_of_items = result[i].number_of_items;
				var item_condition = result[i].item_condition;
				var description = result[i].description;
				var image_url = result[i].image_url == "" ? "default_picture.jpg" : result[i].image_url;
				var date_item = result[i].date_item;
				var parent_category = result[i].parent_category;
				var sub_category = result[i].sub_category;
				var item_county = result[i].item_county;
				var item_locality = result[i].item_locality;
				var item_description = '<b> Kategooria: </b>' + parent_category + ' - ' + sub_category + '<br>';
				item_description += '<b> Eseme nimetus: </b>' + item_name + '<br>';
				item_description += '<b> Kogus: </b>' + number_of_items + '<br>';
				item_description += '<b> Asukoht: </b>' + item_county + ', ' + item_locality + '<br>';
				item_description += '<a href = "/jagamewebapp/web/item?item_id=' + id + '">Vaata lähemalt</a>';
				
				var rowSwitch = '';
				if (i > 0 && i % 3 == 0) {
					rowSwitch = '</div><div class="row">';							
				}
				allItemsContainer += rowSwitch + '<div class="col-md-4"><div class="panel panel-primary"><div class="panel-body"><img style=height:200px; width:200px src="/jagamewebapp/rest/get_image?image_name=' + image_url + '" class="img-responsive" style="width:100%" alt="Image"></div><div class="panel-footer">' + item_description + '</div></div></div>';
			}
			$("#allItems").append(allItemsContainer);
		}
	});
}

//registration functions

function renewUserData() {
	$.ajax({
		type : "get",
		url : "/jagamewebapp/rest/get_authenticated_user",
		success : function(result) {
			currentUserLocationId = result.location_id;
			getLocationToId(currentUserLocationId);
			currentLoggedInUser = result.id;
			$("#registration_first_name").val(result.first_name);
			$("#registration_last_name").val(result.last_name);
			$("#location").val(currentLocationName);
			$("#registration_user_name").val(result.user_name);
			$("#registration_password").val(result.password);
			$("#registration_phone").val(result.phone);
			$("#registration_email").val(result.email);
		} 	
	});
}

function userRegistrate(){
	$("#passwordMatch").hide();
	$("#fillOutEveryRow").hide();
	$("#renameUser").hide();
	if (($("#registration_password").val()!=$("#registration_password2").val())||(($("#registration_first_name").val()==""||$("#registration_last_name").val()==""||$("#registration_user_name").val()==""||$("#registration_password").val()==""||$("#registration_password2").val()==""||$("#registration_phone").val()==""||$("#registration_email").val()==""||$("#location").val()==""))) {
		if($("#registration_password").val()!=$("#registration_password2").val()) {
			$("#passwordMatch").show();
		}
		if ($("#registration_first_name").val()==""||$("#registration_last_name").val()==""||$("#registration_user_name").val()==""||$("#registration_password").val()==""||$("#registration_password2").val()==""||$("#registration_phone").val()==""||$("#registration_email").val()==""||$("#location").val()=="") {
			$("#fillOutEveryRow").show();
		}
	} else {
		$.ajax({
			type: "post",
			url:"/jagamewebapp/rest/check_user_name",
			data: {
				"user_name": $("#registration_user_name").val()
				},
				success : function(result) {
					if (result == "available") {
						getIdToLocalities();
						$.ajax({
							type: "post",
							dataType:"json",
							contentType: "application/json",
							url:"/jagamewebapp/rest/add_users",
							data: JSON.stringify({
								"first_name":$("#registration_first_name").val(),
								"last_name":$("#registration_last_name").val(),
								"user_name":$("#registration_user_name").val(),
								"password":$("#registration_password").val(),
								"phone":$("#registration_phone").val(),
								"email":$("#registration_email").val(),
								"location_id":$("#location").val()	
							}),
							complete : function(result) {
								$("#regCompleteModal").modal("show");
							}
						});
					} else {
						$("#renameUser").show();
					}
				}
			});
	}
}

function editUser() {
	if (($("#registration_password").val()!=$("#registration_password2").val())||(($("#registration_first_name").val()==""||$("#registration_last_name").val()==""||$("#registration_user_name").val()==""||$("#registration_password").val()==""||$("#registration_password2").val()==""||$("#registration_phone").val()==""||$("#registration_email").val()==""||$("#location").val()==""))) {
		if($("#registration_password").val()!=$("#registration_password2").val()) {
			$("#passwordMatch").show();
		} else {
			$("#passwordMatch").hide();
		}
		if ($("#registration_first_name").val()==""||$("#registration_last_name").val()==""||$("#registration_user_name").val()==""||$("#registration_password").val()==""||$("#registration_password2").val()==""||$("#registration_phone").val()==""||$("#registration_email").val()==""||$("#location").val()=="") {
			$("#fillOutEveryRow").show();
		} else {
			$("#fillOutEveryRow").hide();
		}
	} else {
		if (currentUserName != $("#registration_user_name").val()) {
		$.ajax({
			type: "post",
			url:"/jagamewebapp/rest/check_user_name",
			data: {
				"user_name": $("#registration_user_name").val()
				},
				success : function(result) {
					if (result == "available") {
						postEdituser();
					} else {
						$("#renameUser").show();
					}
				}
			});
		} else {
			postEdituser();
		}
	}
}

function postEdituser() {
	getIdToLocalities();
	$.ajax({
		type : "post",
		dataType : "json",
		contentType : "application/json",
		url : "/jagamewebapp/rest/edit_user",
		data : JSON.stringify({
			"id" : currentLoggedInUser,
			"first_name" : $("#registration_first_name").val(),
			"last_name" : $("#registration_last_name").val(),
			"user_name" : $("#registration_user_name").val(),
			"password" : $("#registration_password").val(),
			"phone" : $("#registration_phone").val(),
			"email" : $("#registration_email").val(),
			"location_id" : $("#location").val()
		}),
		complete : function(result) {
			$("#regUpdateModal").modal("show");
		}
	});
}

function deleteUserWindow (){
	$("#deleteUserModal").modal("show");
}

function deleteUser() {
	$.ajax({
		type: "post",
		dataType: "json",
		contentType: "application/json",
		url: "/jagamewebapp/rest/delete_user",
		data: JSON.stringify({
			"id":currentLoggedInUser
		}),
		complete: function(result) {
			deleteUserItems();
			userLogOut();
		}
	});
}

function deleteUserItems() {
	$.ajax({
		type : "post",
		dataType : "json",
		contentType : "application/json",
		url : "/jagamewebapp/rest/delete_user_items",
		data : JSON.stringify({
			"user_id": currentLoggedInUser
		})
	});
}

//login functions

function userLogin() {
	$.ajax({
		type:"post",
		url:"/jagamewebapp/rest/authenticate",
		data: {
			"user_name":$("#user_name").val(),
			"password":$("#password").val()
		},
		success : function(result) {
			if (result == "ok") {
				document.location = "/jagamewebapp/web/index";
			} else {
				$("#wrongPassword").show();
			}	
		}
	});
}