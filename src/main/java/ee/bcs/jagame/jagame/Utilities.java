package ee.bcs.jagame.jagame;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Utilities {
	
	private final static String CONNECTION_URL = "jdbc:mysql://localhost:3306/jagame";
	private final static String CONNECTION_USERNAME = "root";
	private final static String CONNECTION_PASSWORD = "mysql";

	public static ResultSet consumeSQL (String sql) {
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			conn = DriverManager.getConnection(CONNECTION_URL, CONNECTION_USERNAME, CONNECTION_PASSWORD);
			stmt = conn.createStatement();
			return stmt.executeQuery(sql);
		} catch (SQLException|ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	return null;
	}

}
