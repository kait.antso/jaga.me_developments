package ee.bcs.jagame.jagame;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.jagame.jagameDAO.Category;

public class CategoryService {
	
	public static Category[] getCategories() {
		List<Category> categories = new ArrayList<>();
		ResultSet results = Utilities.consumeSQL("SELECT * FROM categories WHERE parent_category_id IS NULL");
		try {
			while (results.next()) {
				categories.add(new Category(results));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return categories.toArray(new Category[0]);
	}
	
	public static Category[] getSubCategories(String parentCategoryId) {
		List<Category> subCategories = new ArrayList<>();
		ResultSet results = Utilities.consumeSQL(String.format("SELECT * FROM categories WHERE parent_category_id = %s", parentCategoryId));
		try {
			while (results.next()) {
				subCategories.add(new Category(results));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return subCategories.toArray(new Category[0]);
	}
	
	public static Category[] getSiblingCategories(String categoryId) {
		List<Category> items = new ArrayList<>();
		ResultSet results = Utilities.consumeSQL("SELECT * FROM categories WHERE parent_category_id = (SELECT parent_category_id FROM categories WHERE id = " + categoryId + ")");
		try {
			while (results.next()) {
				items.add(new Category(results));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return items.toArray(new Category[0]);
	}	
	
	public static Category[] getCategoryByItem(String itemId) {
		List<Category> items = new ArrayList<>();
		ResultSet results = Utilities.consumeSQL("SELECT * FROM categories WHERE parent_category_id = (SELECT categories.parent_category_id FROM items INNER JOIN categories ON items.category_id = categories.ID WHERE items.ID= " + itemId + ")");
		try {
			while (results.next()) {
				items.add(new Category(results));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return items.toArray(new Category[0]);
	}	
	
	public static Category getParentCategoryBySubCategory (String categoryId) {
		ResultSet results = Utilities.consumeSQL(String.format("SELECT * FROM categories WHERE id = (SELECT parent_category_id FROM categories WHERE id = %s)", categoryId));
		try {
			while (results.next()) {
				return new Category(results);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
