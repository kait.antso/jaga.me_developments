package ee.bcs.jagame.jagame;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.jagame.jagameDAO.Item;

public class ItemService {
	
	public static Item[] getItems(int locationId) {
		List<Item> items = new ArrayList<>();
		if (locationId != 0) {
			ResultSet results = Utilities.consumeSQL("SELECT items.*, categories_1.category_name, categories.category_name, locations_1.administrative_unit, locations.administrative_unit, users.user_name, users.phone, users.email FROM categories AS categories_1 INNER JOIN (categories INNER JOIN ((locations INNER JOIN (items INNER JOIN users ON items.user_id = users.ID) ON locations.ID = items.location_id) INNER JOIN locations AS locations_1 ON locations.parent_location_id = locations_1.ID) ON categories.ID = items.category_id) ON categories_1.ID = categories.parent_category_id WHERE locations_1.id = " + locationId + " ORDER BY date_item DESC");
			try {
				while (results.next()) {
					items.add(new Item(results));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return items.toArray(new Item[0]);
		} else {
			ResultSet results = Utilities.consumeSQL(
					"SELECT items.*, categories_1.category_name, categories.category_name, locations_1.administrative_unit, locations.administrative_unit, users.user_name, users.phone, users.email FROM categories AS categories_1 INNER JOIN (categories INNER JOIN ((locations INNER JOIN (items INNER JOIN users ON items.user_id = users.ID) ON locations.ID = items.location_id) INNER JOIN locations AS locations_1 ON locations.parent_location_id = locations_1.ID) ON categories.ID = items.category_id) ON categories_1.ID = categories.parent_category_id ORDER BY date_item DESC");
			try {
				while (results.next()) {
					items.add(new Item(results));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return items.toArray(new Item[0]);
		}
	}
	
	public static Item[] getUserItems(int userId) {
		List<Item> items = new ArrayList<>();
		ResultSet results = Utilities.consumeSQL("SELECT items.*, categories_1.category_name, categories.category_name, locations_1.administrative_unit, locations.administrative_unit, users.user_name, users.phone, users.email FROM categories AS categories_1 INNER JOIN (categories INNER JOIN ((locations INNER JOIN (items INNER JOIN users ON items.user_id = users.ID) ON locations.ID = items.location_id) INNER JOIN locations AS locations_1 ON locations.parent_location_id = locations_1.ID) ON categories.ID = items.category_id) ON categories_1.ID = categories.parent_category_id WHERE items.user_id = " + userId + " ORDER BY date_item DESC");
		try {
			while (results.next()) {
				items.add(new Item(results));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return items.toArray(new Item[0]);
	}

	public static Item[] getParentCategoryItems(String parentCategoryid, int locationId) {
		List<Item> items = new ArrayList<>();
		if (locationId != 0) {
			ResultSet results = Utilities.consumeSQL("SELECT items.*, categories_1.category_name, categories.category_name, locations_1.administrative_unit, locations.administrative_unit, users.user_name, users.phone, users.email FROM categories AS categories_1 INNER JOIN (categories INNER JOIN ((locations INNER JOIN (items INNER JOIN users ON items.user_id = users.ID) ON locations.ID = items.location_id) INNER JOIN locations AS locations_1 ON locations.parent_location_id = locations_1.ID) ON categories.ID = items.category_id) ON categories_1.ID = categories.parent_category_id WHERE items.category_id IN (SELECT categories.id FROM categories WHERE categories.parent_category_id = " + parentCategoryid + ") AND locations_1.id = " + locationId + " ORDER BY date_item DESC");
			try {
				while (results.next()) {
					items.add(new Item(results));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return items.toArray(new Item[0]);
		} else {
			ResultSet results = Utilities.consumeSQL("SELECT items.*, categories_1.category_name, categories.category_name, locations_1.administrative_unit, locations.administrative_unit, users.user_name, users.phone, users.email FROM categories AS categories_1 INNER JOIN (categories INNER JOIN ((locations INNER JOIN (items INNER JOIN users ON items.user_id = users.ID) ON locations.ID = items.location_id) INNER JOIN locations AS locations_1 ON locations.parent_location_id = locations_1.ID) ON categories.ID = items.category_id) ON categories_1.ID = categories.parent_category_id WHERE items.category_id IN (SELECT categories.id FROM categories WHERE categories.parent_category_id = " + parentCategoryid + ") ORDER BY date_item DESC");
			try {
				while (results.next()) {
					items.add(new Item(results));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return items.toArray(new Item[0]);
		}
	}
	
	public static Item[] getCategoryItems(String categoryid, int locationId) {
		List<Item> items = new ArrayList<>();
		if (locationId != 0) {
			ResultSet results = Utilities.consumeSQL("SELECT items.*, categories_1.category_name, categories.category_name, locations_1.administrative_unit, locations.administrative_unit, users.user_name, users.phone, users.email FROM categories AS categories_1 INNER JOIN (categories INNER JOIN ((locations INNER JOIN (items INNER JOIN users ON items.user_id = users.ID) ON locations.ID = items.location_id) INNER JOIN locations AS locations_1 ON locations.parent_location_id = locations_1.ID) ON categories.ID = items.category_id) ON categories_1.ID = categories.parent_category_id WHERE category_id = " + categoryid + " AND locations_1.id = " + locationId + " ORDER BY date_item DESC");
			try {
				while (results.next()) {
					items.add(new Item(results));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return items.toArray(new Item[0]);
		} else {
			ResultSet results = Utilities.consumeSQL("SELECT items.*, categories_1.category_name, categories.category_name, locations_1.administrative_unit, locations.administrative_unit, users.user_name, users.phone, users.email FROM categories AS categories_1 INNER JOIN (categories INNER JOIN ((locations INNER JOIN (items INNER JOIN users ON items.user_id = users.ID) ON locations.ID = items.location_id) INNER JOIN locations AS locations_1 ON locations.parent_location_id = locations_1.ID) ON categories.ID = items.category_id) ON categories_1.ID = categories.parent_category_id WHERE category_id = " + categoryid + " ORDER BY date_item DESC");
			try {
				while (results.next()) {
					items.add(new Item(results));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return items.toArray(new Item[0]);
		}
	}
	
	public static Item getItem(String itemId) {
		ResultSet results = Utilities.consumeSQL("SELECT items.*, categories_1.category_name, categories.category_name, locations_1.administrative_unit, locations.administrative_unit, users.user_name, users.phone, users.email FROM categories AS categories_1 INNER JOIN (categories INNER JOIN ((locations INNER JOIN (items INNER JOIN users ON items.user_id = users.ID) ON locations.ID = items.location_id) INNER JOIN locations AS locations_1 ON locations.parent_location_id = locations_1.ID) ON categories.ID = items.category_id) ON categories_1.ID = categories.parent_category_id WHERE items.id = " + itemId + " ORDER BY date_item DESC");
		try {
			while (results.next()) {
				return new Item(results);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void saveItem(Item item) {
		if (item.getId() == 0) {
			String sqlAdd = String.format("INSERT INTO items (category_id, user_id, location_id, item_name, number_of_items, item_condition, deal_type, description, image_url, date_item) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')", item.getCategory_id(), item.getUser_id(), item.getLocation_id(), item.getItem_name(), item.getNumber_of_items(), item.getItem_condition(), item.getDeal_type(), item.getDescription(), item.getImage_url(), item.getDate_item());
			Utilities.consumeSQL(sqlAdd);
		} else {
			String sqlEdit = String.format("UPDATE items SET category_id = '%s', location_id = '%s', item_name = '%s', number_of_items = '%s', item_condition = '%s', deal_type = '%s', description = '%s', image_url = '%s', date_item = '%s' WHERE id = '%s'", item.getCategory_id(), item.getLocation_id(), item.getItem_name(), item.getNumber_of_items(), item.getItem_condition(), item.getDeal_type(), item.getDescription(), item.getImage_url(), item.getDate_item(), item.getId());
			Utilities.consumeSQL(sqlEdit);
		}
	}
	
	public static void deleteItem (Item item) {
		String sql = String.format("DELETE FROM items WHERE id = %s", item.getId());
		Utilities.consumeSQL(sql);
	}
	
	public static void deleteUserItems (Item item) {
		String sql = String.format("DELETE FROM items WHERE user_id = %s", item.getUser_id());
		Utilities.consumeSQL(sql);
	}

}
