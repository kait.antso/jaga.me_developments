package ee.bcs.jagame.jagame;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


import ee.bcs.jagame.jagameDAO.Category;
import ee.bcs.jagame.jagameDAO.Item;
import ee.bcs.jagame.jagameDAO.Location;
import ee.bcs.jagame.jagameDAO.User;

public class LocationService {
	
	//Locations methods
	
	/*
	public static List<Location> getLocationsToItems(int locationId, List<Location> childLocations) {
		List<Location> itemLocation = null;
		if (childLocations != null) {
			itemLocation = childLocations;
		} else {
			itemLocation = new ArrayList<>();
		}
		
		ResultSet results = Utilities.consumeSQL(String.format("SELECT id, administrative_unit, parent_location_id FROM locations WHERE id = '%s'", locationId));
		try {
			while (results.next()) {
				Location location = new Location(results);
				itemLocation.add(location);
				if (location.getParent_location_id() > 0) {
					JagameService.getLocationsToItems(location.getParent_location_id(), itemLocation);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return itemLocation;
	}
	*/	
	
	public static Location[] getSubLocations() {
		List<Location> subLocations = new ArrayList<>();
		ResultSet results = Utilities.consumeSQL("SELECT locations.ID, CONCAT(locations.administrative_unit,\", \", locations_1.administrative_unit) AS location_full_name, locations.parent_location_id FROM locations AS locations_1 INNER JOIN locations ON locations_1.ID = locations.parent_location_id WHERE ((locations.parent_location_id) IS NOT NULL)");
		try {
			while (results.next()) {
				subLocations.add(new Location(results));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return subLocations.toArray(new Location[0]);
	}
	
	public static Location[] getCounties() {
		List<Location> subLocations = new ArrayList<>();
		ResultSet results = Utilities.consumeSQL("SELECT * FROM locations WHERE parent_location_id IS NULL");
		try {
			while (results.next()) {
				subLocations.add(new Location(results));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return subLocations.toArray(new Location[0]);
	}
	
	public static Location getLocation(int locationId) {
		ResultSet results = Utilities.consumeSQL("SELECT * FROM locations WHERE id =" + locationId);
		try {
			while (results.next()) {
				return new Location(results);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
