package ee.bcs.jagame.jagame;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.jagame.jagameDAO.User;

public class UserService {
	
	public static User getUser (String userName, String password) {
		ResultSet result = Utilities.consumeSQL("SELECT * FROM users WHERE user_name = '" + userName + "' AND password = '" + password + "'");
		try {
			if (result.next()) {
				return new User(result);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static User getUser(int id) {
		ResultSet result = Utilities.consumeSQL("SELECT * FROM users WHERE id =" + id);
		try {
			if (result.next()) {
				return new User(result);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static User getUser(String userName) {
		ResultSet result = Utilities.consumeSQL("SELECT * FROM users WHERE user_name ='" + userName + "'");
		try {
			if (result.next()) {
				return new User(result);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void addUsers (User user) {
		String sql = String.format("INSERT INTO users (first_name, last_name, user_name, password, phone, email, location_id) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')", user.getFirst_name(), user.getLast_name(), user.getUser_name(), user.getPassword(), user.getPhone(), user.getEmail(), user.getLocation_id());
		Utilities.consumeSQL(sql);
	}
	
	public static void editUsers (User user) {
		String sql = String.format("UPDATE users SET first_name = '%s', last_name = '%s', user_name = '%s', password = '%s', phone = '%s', email = '%s', location_id = '%s' WHERE id = '%s'", user.getFirst_name(), user.getLast_name(), user.getUser_name(), user.getPassword(), user.getPhone(), user.getEmail(), user.getLocation_id(), user.getId());
		Utilities.consumeSQL(sql);
	}

	public static void deleteUser(User user) {
		String sql = String.format("DELETE FROM users WHERE id = %s", user.getId());
		Utilities.consumeSQL(sql);
	}
	
}
