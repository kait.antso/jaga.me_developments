package ee.bcs.jagame.web;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.server.mvc.Viewable;

@Path("/")
public class JagameWebResources {
	
	@GET
	@Path("/index")
	@Produces(MediaType.TEXT_HTML)
	public Viewable index () {
		Map<String, String> map = new HashMap<>();
		map.put("page", "index");
		return new Viewable ("/jagame_template.ftl", map);
	}
	
	@GET
	@Path("/subcategory")
	@Produces(MediaType.TEXT_HTML)
	public Viewable subCategory () {
		Map<String, String> map = new HashMap<>();
		map.put("page", "subCategory");
		return new Viewable ("/jagame_template.ftl", map);
	}
	
	@GET
	@Path("/parentcategory")
	@Produces(MediaType.TEXT_HTML)
	public Viewable parentCategory () {
		Map<String, String> map = new HashMap<>();
		map.put("page", "parentCategory");
		return new Viewable ("/jagame_template.ftl", map);
	}

	@GET
	@Path("/user_items")
	@Produces(MediaType.TEXT_HTML)
	public Viewable userItems () {
		Map<String, String> map = new HashMap<>();
		map.put("page", "userItems");
		return new Viewable ("/jagame_template.ftl", map);
	}

	@GET
	@Path("/my_items")
	@Produces(MediaType.TEXT_HTML)
	public Viewable myItems () {
		Map<String, String> map = new HashMap<>();
		map.put("page", "myItems");
		return new Viewable ("/jagame_template.ftl", map);
	}
	
	@GET
	@Path("/item")
	@Produces(MediaType.TEXT_HTML)
	public Viewable item () {
		Map<String, String> map = new HashMap<>();
		map.put("page", "item");
		return new Viewable ("/jagame_template.ftl", map);
	}
	
	@GET
	@Path("/login")
	@Produces(MediaType.TEXT_HTML)
	public Viewable logIn () {
		Map<String, String> map = new HashMap<>();
		map.put("page", "logIn");
		return new Viewable ("/jagame_template.ftl", map);
	}
	
	@GET
	@Path("/registration")
	@Produces(MediaType.TEXT_HTML)
	public Viewable registration () {
		Map<String, String> map = new HashMap<>();
		map.put("page", "registration");
		return new Viewable ("/jagame_template.ftl", map);
	}
	
}
