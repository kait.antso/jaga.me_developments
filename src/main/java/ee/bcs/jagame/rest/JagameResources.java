package ee.bcs.jagame.rest;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import ee.bcs.jagame.jagame.CategoryService;
import ee.bcs.jagame.jagame.ItemService;
import ee.bcs.jagame.jagame.LocationService;
import ee.bcs.jagame.jagame.UserService;
import ee.bcs.jagame.jagameDAO.Category;
import ee.bcs.jagame.jagameDAO.Item;
import ee.bcs.jagame.jagameDAO.Location;
import ee.bcs.jagame.jagameDAO.User;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;

@Path("/")
public class JagameResources {
	
	//Users
	@GET
	@Path("/get_authenticated_user")
	@Produces(MediaType.APPLICATION_JSON)
	public User getAuthUser(@Context HttpServletRequest req) {
		HttpSession session= req.getSession(true);
		User currentUser = (User) session.getAttribute("user");
		if (currentUser != null) {
			return currentUser;
		} else {
			return new User();
		}
	}
	
	@GET
	@Path("/get_authenticated_user_items")
	@Produces(MediaType.APPLICATION_JSON)
	public Item[] getAuthenticatedUserItems(@Context HttpServletRequest req) {
		HttpSession session= req.getSession(true);
		User currentUser = (User) session.getAttribute("user");
		return ItemService.getUserItems(currentUser.getId());
	}
	
	@GET
	@Path("/get_all_user_items")
	@Produces(MediaType.APPLICATION_JSON)
	public Item[] allItems (@QueryParam("user_id") int userId) {
		return ItemService.getUserItems(userId);
	}
	
	@POST
	@Path("/add_users")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String addUsers (User user) {
		UserService.addUsers(user);
		return null;
	}
	@POST
	@Path("/edit_user")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String editUsers(@Context HttpServletRequest req, User user) {
		UserService.editUsers(user);
		HttpSession session = req.getSession(true);
		User currentUser = UserService.getUser(user.getId());
		session.setAttribute("user", currentUser);
		return null;
	}
	
	@POST
	@Path("/check_user_name")
	@Produces(MediaType.TEXT_PLAIN)
	public String checkUserName(@FormParam("user_name") String userName) {
		User userNameCheck = UserService.getUser(userName);
		if (userNameCheck == null) {
			return "available";
		} else {
			return "unavailable";
		}
	}
	
	@POST
	@Path("/delete_user")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String deleteUser(User user) {
		UserService.deleteUser(user);
		return null;
	}
	
	@POST
	@Path("/authenticate")
	@Produces(MediaType.TEXT_PLAIN)
	public String authenticate(@Context HttpServletRequest req, @FormParam("user_name") String userName,
			@FormParam("password") String password) {
		User currentUser = UserService.getUser(userName, password);
		if (currentUser == null) {
			HttpSession session = req.getSession(true);
			session.setAttribute("user", currentUser);
			return "notLoggedIn";
		}
		HttpSession session = req.getSession(true);
		session.setAttribute("user", currentUser);
		return "ok";
	}
	
	//Categories
	@GET
	@Path("/get_allcategories")
	@Produces(MediaType.APPLICATION_JSON)
	public Category[] getCategories() {
		return CategoryService.getCategories();
	}
	@GET
	@Path("/get_subcategories")
	@Produces(MediaType.APPLICATION_JSON)
	public Category[] getSubCategories (@QueryParam("parent_category_id") String parentCategoryId) {
		return CategoryService.getSubCategories(parentCategoryId);
	}

	@GET
	@Path("/get_parentcategory_items")
	@Produces(MediaType.APPLICATION_JSON)
	public Item[] getParentCategoryItems(@Context HttpServletRequest req, @QueryParam("parent_category_id") String parentCategoryid) {
		HttpSession countySelection= req.getSession(true);
		Location currentLocation = (Location) countySelection.getAttribute("county");
		if (currentLocation != null) {
			return ItemService.getParentCategoryItems(parentCategoryid, currentLocation.getId());
		} else {
			return ItemService.getParentCategoryItems(parentCategoryid, 0);
		}
	}
	
	@GET
	@Path("/get_category_items")
	@Produces(MediaType.APPLICATION_JSON)
	public Item[] getCategoryItems(@Context HttpServletRequest req, @QueryParam("category_id") String categoryid) {
		HttpSession countySelection= req.getSession(true);
		Location currentLocation = (Location) countySelection.getAttribute("county");
		if (currentLocation != null) {
			return ItemService.getCategoryItems(categoryid, currentLocation.getId());
		} else {
			return ItemService.getCategoryItems(categoryid, 0);
		}
	}
	
	@GET
	@Path("/get_sibling_categories")
	@Produces(MediaType.APPLICATION_JSON)
	public Category[] getCategoryBySiblingCategory(@QueryParam("category_id") String categoryId) {
		return CategoryService.getSiblingCategories(categoryId);
	}
		
	@GET
	@Path("/get_category_by_item")
	@Produces(MediaType.APPLICATION_JSON)
	public Category[] getCategoryByItem(@QueryParam("item_id") String itemId) {
		return CategoryService.getCategoryByItem(itemId);
	}
	
	@GET
	@Path("/get_parentcategory_by_subcategory")
	@Produces(MediaType.APPLICATION_JSON)
	public Category getCategoryByParentcategory(@QueryParam("category_id") String categoryId) {
		return CategoryService.getParentCategoryBySubCategory(categoryId);
	}
	
	//Items
	@GET
	@Path("/get_items")
	@Produces(MediaType.APPLICATION_JSON)
	public Item[] getItems(@Context HttpServletRequest req) {
		HttpSession countySelection= req.getSession(true);
		Location currentLocation = (Location) countySelection.getAttribute("county");
		if (currentLocation != null) {
			return ItemService.getItems(currentLocation.getId());
		} else {
			return ItemService.getItems(0);
		}	
	}
	
	@GET
	@Path("/get_item")
	@Produces(MediaType.APPLICATION_JSON)
	public Item getItem (@QueryParam("item_id") String itemId) {
		return ItemService.getItem(itemId);
	}
	
	@POST
	@Path("/delete_item")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String deleteItem(Item item) {
		ItemService.deleteItem(item);
		return null;
	}
	
	@POST
	@Path("/delete_user_items")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String deleteUserItems(Item item) {
		ItemService.deleteUserItems(item);
		return null;
	}
	
	//Locations
	@GET
	@Path("/get_locations")
	@Produces(MediaType.APPLICATION_JSON)
	public Location[] getLocations() {
		return LocationService.getSubLocations();
	}
	
	@GET
	@Path("/get_counties")
	@Produces(MediaType.APPLICATION_JSON)
	public Location[] getCounties() {
		return LocationService.getCounties();
	}
	
	@POST
	@Path("/set_county")
	@Produces(MediaType.TEXT_PLAIN)
	public String setLocation(@Context HttpServletRequest req, @FormParam("location_id") int locationId) {
		Location currentLocation = LocationService.getLocation(locationId);
		HttpSession countySelection = req.getSession(true);
		
		countySelection.setAttribute("county", currentLocation);
		return "ok";
	}
	
	@GET
	@Path("/get_current_county")
	@Produces(MediaType.APPLICATION_JSON)
	public Location getCurrentLocation(@Context HttpServletRequest req) {
		HttpSession countySelection= req.getSession(true);
		Location currentLocation = (Location) countySelection.getAttribute("county");
		if (currentLocation != null) {
			return currentLocation;
		} else {
			return new Location();
		}
	}

	// Picture/file import
	
	@GET
	@Path("/get_image")
	@Produces("image/jpg")
	public Response getFullImage(@QueryParam("image_name") String imageName) throws IOException {
		BufferedImage image = null;
		image = ImageIO.read(new File("C:/temp/jagame_images/" + imageName + " "));
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(image, "jpg", baos);
		byte[] imageData = baos.toByteArray();
		return Response.ok(imageData).build();
	}
	
	@POST
	@Path("/save_item")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public String saveItem(@Context HttpServletRequest req, @FormDataParam("modalChildCategories") int categoryId,
			@FormDataParam("location") int locationId, @FormDataParam("item_name") String itemName,
			@FormDataParam("number_of_items") int numberOfItems, @FormDataParam("item_condition") String itemCondition,
			@FormDataParam("deal_type") String dealType, @FormDataParam("description") String description,
			@FormDataParam("item_id") int itemId, @FormDataParam("image_url") InputStream image,
			@FormDataParam("image_url") FormDataContentDisposition imageData) {
		HttpSession session = req.getSession(true);
		User currentUser = (User) session.getAttribute("user");

		if (currentUser != null) {

			String fileName = UUID.randomUUID().toString() + ".jpg";
			boolean imageExists = false;
			
			String UPLOAD_PATH = "C:/temp/jagame_images/";
			try {
				int read = 0;
				byte[] bytes = new byte[1024];

				OutputStream out = new FileOutputStream(new File(UPLOAD_PATH + fileName));
				while ((read = image.read(bytes)) != -1) {
					imageExists = true;
					out.write(bytes, 0, read);
				}
				out.flush();
				out.close();
			} catch (IOException e) {
				throw new WebApplicationException("Error while uploading file. Please try again !!");
			}
			
			Item currentItem = ItemService.getItem(String.valueOf(itemId));
			
			Item item = new Item();
			item.setUser_id(currentUser.getId());
			item.setId(itemId);
			item.setCategory_id(categoryId);
			item.setLocation_id(locationId);
			item.setItem_name(itemName);
			item.setNumber_of_items(numberOfItems);
			item.setItem_condition(itemCondition);
			item.setDeal_type(dealType);
			item.setDescription(description);
			if (imageExists) {
				item.setImage_url(fileName);
			} else  {
				if (currentItem != null) {
					item.setImage_url(currentItem.getImage_url());
				}
			}

			ItemService.saveItem(item);
			return "OK";
		} else {
			return "NOK";
		}
	}
}

