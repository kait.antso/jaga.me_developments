package ee.bcs.jagame.jagameDAO;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Category {
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String name) {
		this.category_name = name;
	}
	public int getparent_category_id() {
		return parent_category_id;
	}
	public void setparent_category_id(int parent_category_id) {
		this.parent_category_id = parent_category_id;
	}
	
	private int id;
	private String category_name;
	private int parent_category_id;
	
	public Category() {};
	
	public Category(ResultSet categoryData) {
		try {
			this.id = categoryData.getInt(1);
			this.category_name = categoryData.getString(2);
			this.parent_category_id = categoryData.getInt(3);
		} catch (SQLException e) {
			e.printStackTrace();
		}	
	}
}
