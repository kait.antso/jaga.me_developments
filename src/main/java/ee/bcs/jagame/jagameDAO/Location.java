package ee.bcs.jagame.jagameDAO;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Location {
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAdministrative_unit() {
		return administrative_unit;
	}
	public void setAdministrative_unit(String administrative_unit) {
		this.administrative_unit = administrative_unit;
	}
	public int getParent_location_id() {
		return parent_location_id;
	}
	public void setParent_location_id(int parent_location_id) {
		this.parent_location_id = parent_location_id;
	}
	private int id;
	private String administrative_unit="";
	private int parent_location_id=0;
	
	public Location() {}
	public Location(ResultSet locationData) {
		try {
			this.id = locationData.getInt(1);
			this.administrative_unit = locationData.getString(2);
			this.parent_location_id = locationData.getInt(3);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
