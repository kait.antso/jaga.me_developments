package ee.bcs.jagame.jagameDAO;

import java.sql.ResultSet;
import java.sql.SQLException;

public class User {
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getLocation_id() {
		return location_id;
	}
	public void setLocation_id(int location_id) {
		this.location_id = location_id;
	}
	private int id;
	private String first_name;
	private String last_name;
	private String user_name;
	private String password;
	private String phone;
	private String email;
	private int location_id;
	
	public User () {};
	public User (ResultSet userData) {
		try {
			this.id = userData.getInt(1);
			this.first_name = userData.getString(2);
			this.last_name = userData.getString(3);
			this.user_name = userData.getString(4);
			this.password = userData.getString(5);
			this.phone = userData.getString(6);
			this.email = userData.getString(7);
			this.location_id = userData.getInt(8);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
