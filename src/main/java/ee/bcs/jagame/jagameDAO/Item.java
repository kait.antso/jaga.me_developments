package ee.bcs.jagame.jagameDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Item {
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getLocation_id() {
		return location_id;
	}
	public void setLocation_id(int location_id) {
		this.location_id = location_id;
	}
	public String getItem_name() {
		return item_name;
	}
	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}
	public int getNumber_of_items() {
		return number_of_items;
	}
	public void setNumber_of_items(int number_of_items) {
		this.number_of_items = number_of_items;
	}
	public String getItem_condition() {
		return item_condition;
	}
	public void setItem_condition(String condition) {
		this.item_condition = condition;
	}
	public String getDeal_type() {
		return deal_type;
	}
	public void setDeal_type(String deal_type) {
		this.deal_type = deal_type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
	public String getDate_item() {
		return date_item;
	}
	public void setDate_item(String date_item) {
		this.date_item = date_item;
	}
	public String getParent_category() {
		return parent_category;
	}
	public void setParent_category(String parent_category) {
		this.parent_category = parent_category;
	}
	public String getSub_category() {
		return sub_category;
	}
	public void setSub_category(String sub_category) {
		this.sub_category = sub_category;
	}
	public String getItem_county() {
		return item_county;
	}
	public void setItem_county(String item_county) {
		this.item_county = item_county;
	}
	public String getItem_locality() {
		return item_locality;
	}
	public void setItem_locality(String item_locality) {
		this.item_locality = item_locality;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_phone() {
		return user_phone;
	}
	public void setUser_phone(String user_phone) {
		this.user_phone = user_phone;
	}
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	
//	public Location[] getLocations() {
//		return locations;
//	}
//	public void setLocations(Location[] locations) {
//		this.locations = locations;
//	}
	DateTimeFormatter formatDate = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:00");
	LocalDateTime today = LocalDateTime.now();
	String date = formatDate.format(today);
	private String date_item=date;
	private int id;
	private int category_id;
	private int user_id;
	private int location_id;
	private String item_name;
	private int number_of_items;
	private String item_condition="";
	private String deal_type="";
	private String description="";
	private String image_url="";
//	private Location[] locations;
	private String parent_category;
	private String sub_category;
	private String item_county;
	private String item_locality;
	private String user_name;
	private String user_phone;
	private String user_email;

	public Item() {};
	public Item(ResultSet itemData) {
		try {
			this.id = itemData.getInt(1);
			this.category_id = itemData.getInt(2);
			this.user_id = itemData.getInt(3);
			this.location_id = itemData.getInt(4);
			this.item_name = itemData.getString(5);
			this.number_of_items = itemData.getInt(6);
			this.item_condition = itemData.getString(7);
			this.deal_type = itemData.getString(8);
			this.description = itemData.getString(9);
			this.image_url = itemData.getString(10);
			this.date_item = itemData.getString(11);
			
			this.parent_category = itemData.getString(12);
			this.sub_category = itemData.getString(13);
			this.item_county = itemData.getString(14);
			this.item_locality = itemData.getString(15);
			this.user_name = itemData.getString(16);
			this.user_phone = itemData.getString(17);
			this.user_email = itemData.getString(18);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
